package com.vazhasapp.assignment21

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.vazhasapp.assignment21.adapter.MenuAdapter
import com.vazhasapp.assignment21.databinding.ActivityMainBinding
import com.vazhasapp.assignment21.fragments.HomeFragment
import com.vazhasapp.assignment21.model.DrawerMenuItem
import com.vazhasapp.assignment21.serverRequest.ServerRequest

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    private val menuRecyclerAdapter = MenuAdapter()
    private val dummyData = mutableListOf<DrawerMenuItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarDrawer.customToolBar)

        init()
    }

    private fun init() {
        setupRecycler()
        setData()
        navigateWithDrawerMenu()
        setupDrawerMenu()
    }

    private fun setupRecycler() {
        binding.drawerRecycler.apply {
            adapter = menuRecyclerAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
    }
    private fun setData() {
        menuRecyclerAdapter.setData(fillData())
    }

    private fun setupDrawerMenu() {
        val navView: NavigationView = binding.navView
        navController = findNavController(R.id.navHostFragment)
        appBarConfiguration = AppBarConfiguration(navController.graph)

        appBarConfiguration = AppBarConfiguration(
            navController.graph
        )

        navView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }
    private fun navigateWithDrawerMenu() {
        menuRecyclerAdapter.menuItemClick = {
            binding.drawerMenu.closeDrawer(GravityCompat.START)

            when(it) {
                0 -> supportFragmentManager.beginTransaction().show(supportFragmentManager.findFragmentById(R.id.homeFragment)!!)
                1 -> supportFragmentManager.beginTransaction().show(supportFragmentManager.findFragmentById(R.id.favoritesFragment)!!)
                2 -> supportFragmentManager.beginTransaction().show(supportFragmentManager.findFragmentById(R.id.topFragment)!!)
                3 -> supportFragmentManager.beginTransaction().show(supportFragmentManager.findFragmentById(R.id.musicFragment)!!)
            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, null)
    }

    private fun fillData(): MutableList<DrawerMenuItem> {
        dummyData.add(DrawerMenuItem(R.drawable.ic_home, "Home"))
        dummyData.add(DrawerMenuItem(R.drawable.ic_favorite, "Favorite"))
        dummyData.add(DrawerMenuItem(R.drawable.ic_music, "Music"))
        dummyData.add(DrawerMenuItem(R.drawable.ic_top, "Top"))

        return dummyData
    }
}