package com.vazhasapp.assignment21.model

data class DrawerMenuItem(
    val itemIcon: Int,
    val itemTitle: String,
)
