package com.vazhasapp.assignment21.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.assignment21.databinding.DrawerMenuItemBinding
import com.vazhasapp.assignment21.model.DrawerMenuItem

typealias MenuItemClick = (position: Int) -> Unit

class MenuAdapter : RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    private val menuItemList = mutableListOf<DrawerMenuItem>()

    private var selectedMenuItem = 0

    lateinit var menuItemClick: MenuItemClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MenuViewHolder(
            DrawerMenuItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = menuItemList.size

    inner class MenuViewHolder(private val binding: DrawerMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
            private lateinit var currentMenuItem: DrawerMenuItem

            fun bind() {
                currentMenuItem = menuItemList[adapterPosition]

                binding.imMenuIcon.setImageResource(currentMenuItem.itemIcon)
                binding.tvMenuTitle.text = currentMenuItem.itemTitle
                binding.root.setOnClickListener(this)

                if (selectedMenuItem == adapterPosition) {
                    binding.root.setBackgroundColor(Color.LTGRAY)
                } else {
                    binding.root.setBackgroundColor(Color.WHITE)
                }
            }

        override fun onClick(v: View?) {
            selectedMenuItem = adapterPosition
            menuItemClick(adapterPosition)
            notifyDataSetChanged()
        }
    }

    fun setData(enteredList: MutableList<DrawerMenuItem>) {
        menuItemList.clear()
        menuItemList.addAll(enteredList)
        notifyDataSetChanged()
    }
}