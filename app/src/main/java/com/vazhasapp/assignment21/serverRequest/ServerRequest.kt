package com.vazhasapp.assignment21.serverRequest

import android.util.Log.d
import kotlinx.coroutines.*

class ServerRequest {

    private val dispatcherIO: CoroutineDispatcher = Dispatchers.IO

   suspend fun onLocationChangedRequest() = withContext(dispatcherIO) {
       while (isActive) {
           d("RequestLog", "Request Sent Successfully")
           delay(5000)
       }
   }
}